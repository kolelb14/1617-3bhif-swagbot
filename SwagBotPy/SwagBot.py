
from communication import Communication

# Settings
UDP_IP = "0.0.0.0"
UDP_PORT = 50888
NAME = "Sw4gB0t"

SERVOS = {"left": [0, 1],
          "right": [2, 3]}

def main():
    """The Main Function"""
    comm = Communication(UDP_IP, UDP_PORT, NAME)
    comm.init_move_controller(SERVOS)
    commg
    comm.start_check_for_recv()
    comm.client_recv()

if __name__ == '__main__':
    main()
