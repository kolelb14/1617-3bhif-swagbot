
import socket
import time
import threading
from movement import MoveController

class Communication(object):
    """Class for communication between Bot and Client"""
    def __init__(self, udp_ip, udp_port, name="Swag/SC-0-Bot"):
        self.__udp_ip = udp_ip
        self.__udp_port = udp_port
        self.__move_controller = None
        self.__lastrectime = time.time()
        self.name = name

    def init_move_controller(self, servos):
        """Initializes the Move Controller"""
        self.__move_controller = MoveController(servos)

    def client_recv(self):
        """Receives commands from Client(s)"""
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.__udp_ip, self.__udp_port))
        print "UDP socket created"
        while True:
            data, addr = sock.recvfrom(1024)
            print "Received:", data, "From:", addr
            if self.parse(data):
                self.send(sock, addr, self.name + ":" + "ok")
                self.__lastrectime = time.time()
                print "ok"
            else:
                self.send(sock, addr, self.name + ":" + "error")

    def parse(self, msg):
        """Parses the message from the Client"""
        if msg[0] == '$':
            try:
                if msg[1] == 'M':
                    l_r = msg[2:].split(':')
                    self.__move_controller.move(int(l_r[0]), int(l_r[1]))
                elif msg[1] == 'R':
                    self.__move_controller.rotate(int(msg[2:]))
                elif msg == "PING":
                    pass
                else:
                    raise AttributeError("Invalid Message")
            except (AttributeError, IndexError, ValueError) as err:
                print err
                return False
        elif msg[0] == '?':
            print "? is not supported yet"
        return True

    def send(self, sock, addr, msg):
        """Sends a message to the Client"""
        sock.sendto(msg, (addr[0], self.__udp_port))

    def start_bcast(self):
        """Starts the Broadcasting Daemon (For visibility in the Network)"""
        t_bcast = threading.Thread(target=self.broadcast)
        t_bcast.daemon = True
        t_bcast.start()

    def start_check_for_recv(self):
        """Starts the Daemon for checking if data is received"""
        t_bcast = threading.Thread(target=self.check_for_recv)
        t_bcast.daemon = True
        t_bcast.start()

    def check_for_recv(self):
        """checks if data is received"""
        while True:
            since_last_rec = time.time() - self.__lastrectime
            if since_last_rec > 0.5 and self.__move_controller.is_moving():
                print "Nothing received since 0.5 Seconds, stopping the Bot"
                self.__move_controller.stop()
            time.sleep(0.5)

    def broadcast(self):
        """Broadcasts IP, Port and Name"""
        bcast_text = "SWAG" + "\\" + self.name + "\\" + str(self.__udp_port)
        bcast_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        bcast_s.bind(('', 0))
        bcast_s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        count = 0
        while True:
            bcast_s.sendto(bcast_text, ('<broadcast>', 50400))
            print "Broadcasted Data", "No.:", str(count)
            count += 1
            time.sleep(1)
