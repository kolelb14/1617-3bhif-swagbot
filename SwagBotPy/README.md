In order to use SwagBotPy, please install WiringPi-Python before:

https://github.com/WiringPi/WiringPi-Python

AND some python packages:

sudo apt-get install python-dev python-setuptools swig
