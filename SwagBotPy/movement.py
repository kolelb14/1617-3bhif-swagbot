
import wiringpi

class Servo(object):
    """This class represents one of the Servos"""
    def __init__(self, pin1, pin2):
        self.__pin1 = pin1
        self.__pin2 = pin2
        self.__maxval = 100
        self.__val = 0
        wiringpi.softPwmCreate(self.__pin1, 0, self.__maxval)
        wiringpi.softPwmCreate(self.__pin2, 0, self.__maxval)

    def set_speed(self, val):
        """Sets the speed of the servo (-100 to 100)"""
        print val
        if val > 0 and val <= self.__maxval:
            wiringpi.softPwmWrite(self.__pin1, 0)
            wiringpi.softPwmWrite(self.__pin2, val)
        elif val <= 0 and val >= -(self.__maxval):
            wiringpi.softPwmWrite(self.__pin1, -val)
            wiringpi.softPwmWrite(self.__pin2, 0)
        else:
            error = AttributeError()
            error.message = "Invalid attribute 'val'!"
            raise error
        self.__val = val

    def brake(self):
        """Brakes the motor"""
        wiringpi.softPwmWrite(self.__pin1, self.__maxval)
        wiringpi.softPwmWrite(self.__pin2, self.__maxval)
        self.__val = 0

    def stop(self):
        """Stops the motor"""
        wiringpi.softPwmWrite(self.__pin1, 0)
        wiringpi.softPwmWrite(self.__pin2, 0)
        self.__val = 0

    def is_moving(self):
        """Returns True if the Servo is moving, else False"""
        return self.__val is not 0


class MoveController(object):
    """This is the controller for the movement"""
    def __init__(self, config):
        wiringpi.wiringPiSetup()
        self.__servo1 = Servo(config["left"][0], config["left"][1])
        self.__servo2 = Servo(config["right"][0], config["right"][1])

    def move(self, left, right):
        """Sets the spevaled for both servos"""
        print left, right
        self.__servo1.set_speed(left)
        self.__servo2.set_speed(right)

    def rotate(self, val):
        """Rotates the Bot (+ -> left; - -> right)"""
        self.__servo1.set_speed(val)
        self.__servo2.set_speed(-val)

    def brake(self):
        """Brakes the Bot"""
        self.__servo1.brake()
        self.__servo2.brake()

    def stop(self):
        """Stops the Bot"""
        self.__servo1.stop()
        self.__servo2.stop()

    def is_moving(self):
        """Returns True if the Bot is moving"""
        return self.__servo1.is_moving() or self.__servo2.is_moving()
