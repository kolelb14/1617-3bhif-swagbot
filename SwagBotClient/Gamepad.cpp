#include "Gamepad.h"

Gamepad::Gamepad() : x_axis(0), y_axis(0) {

}

float Gamepad::xAxis() const {
    return this->x_axis;
}

float Gamepad::yAxis() const {
    return this->y_axis;
}

float Gamepad::rotation() const {
    return this->the_rotation;
}

void Gamepad::setXAxis(float value) {
    this->x_axis = value;
}

void Gamepad::setYAxis(float value) {
    this->y_axis = value;
}

void Gamepad::setRotation(float value){
    this->the_rotation = value;
}
