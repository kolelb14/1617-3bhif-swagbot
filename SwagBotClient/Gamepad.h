#pragma once

class Gamepad {
public:
    Gamepad();
    float xAxis() const;
    float yAxis() const;
    float rotation() const;
    void setXAxis(float);
    void setYAxis(float);
    void setRotation(float);
private:
    float y_axis; // Speed Forward
    float x_axis; // Curve
    float the_rotation;
};
