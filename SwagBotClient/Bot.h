#pragma once
#include <QtNetwork>
#include <QDateTime>

class Bot {
public:
    explicit Bot(QString name, QHostAddress address,
                 quint16 port);
    bool isEqualTo(Bot * bot) const;
    QString name() const;
    QHostAddress address() const;
    quint16 port() const;
    bool isValid() const;
    bool isDelayed() const;
    void updateTimestamp();
private:
    QString _name;
    QHostAddress _address;
    quint16 _port;
    qint64 _timestamp;
};
