#include "Communication.h"

Communication::Communication(QObject * parent) :
    QObject(parent),
    socket(new QUdpSocket),
    clientSocket(new QUdpSocket),
    bots(new QList<Bot*>),
    doSearch(true),
    timer(new QTimer),
    updatingList(false),
    connectedBot(nullptr),
    gamepad(new Gamepad),
    sendTimer(new QTimer),
    errorMsgString(QString(""))
{
    socket->bind(QHostAddress::Any, 50400);
    if(!connect(socket, SIGNAL(readyRead()), this, SLOT(dataIncoming()))){
        errorMsgString = QString("socket bind error");
    }

    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateList()));
    timer->start();

    connect(sendTimer, SIGNAL(timeout()), this, SLOT(sendData()));
    sendTimer->setInterval(20);
    sendTimer->start();
}

Communication::~Communication() {
    delete socket;
    removeConnectedBot();
    for (int i = 0; i < bots->length(); ++i) {
        delete bots->at(i);
    }
    delete bots;
    delete timer;
    delete gamepad;
    delete sendTimer;
    delete clientSocket;
}

void Communication::searchForClients(bool doSearch) {
    this->doSearch = doSearch;
    if(!this->doSearch) {
        bots->clear();
    } else {
        qDebug() << "Now searching for Clients";
    }
}

void Communication::connectToBot(QString name) {
    removeConnectedBot();
    QStringList split = name.split(QString(" - "));
    for (int i = 0; i < bots->length(); ++i) {
        if(bots->at(i)->name() == split[0] && bots->at(i)->address() == QHostAddress(split[1])){
            qDebug() << "Connected!!";
            connectedBot = new Bot(*bots->at(i));
        }
    }
    if(connectedBot != nullptr) {
        clientSocket->bind(QHostAddress::Any, connectedBot->port());
        connect(clientSocket, SIGNAL(readyRead()), this, SLOT(dataIncomingClient()));
    }
}

void Communication::connectToBot(QString ip, QString port) {
    removeConnectedBot();
    QHostAddress addr(ip);
    quint16 port_i = static_cast<quint16>(port.toInt());
    connectedBot = new Bot(addr.toString(), addr, port_i);
    clientSocket->bind(QHostAddress::Any, connectedBot->port());
    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(dataIncomingClient()));
    qDebug() << "!!!Connected!!!";
}

QStringList Communication::getClientsStringList() {
    QStringList lst;
    for (int i = 0; i < bots->length(); ++i) {
        Bot * bot = bots->at(i);
        lst.append(bot->name() + QString(" - ") + bot->address().toString());
        lst.at(i);
    }
    return lst;
}

void Communication::setGamepadY(float y_axis) {
    gamepad->setYAxis(y_axis);
}

void Communication::setGamepadX(float x_axis) {
    gamepad->setXAxis(x_axis);
}

void Communication::setGamepadRotation(float rot) {
    gamepad->setRotation(rot);
}

void Communication::removeConnectedBot() {
    if(connectedBot != nullptr && !bots->contains(connectedBot)){
        delete connectedBot;
    }
    connectedBot = nullptr;
    if(clientSocket != nullptr){
        clientSocket->close();
        delete clientSocket;
        clientSocket = new QUdpSocket;
    }
}

QString Communication::clientIP() {
    if(this->connectedBot != nullptr) {
        QString str = this->connectedBot->address().toString();
        return QString("http://[") + str + QString("]");
    }
}

QString Communication::errorMsg(){
    return this->errorMsgString;
}

void Communication::delErrorMsg(){
    errorMsgString = QString("");
}

void Communication::insert(QList<Bot*> * bots, Bot * bot) {
    bool found = false;
    for (int i = 0; i < bots->length(); ++i) {
        if(bots->at(i)->isEqualTo(bot)){
            found = true;
            bots->at(i)->updateTimestamp();
            qDebug() << "updated";
            break;
        }
    }
    if(!found){
        bots->append(bot);
        qDebug() << "new Inerted";
    }
}

void Communication::updateList() {
    updatingList = true;
    for (int i = bots->length() - 1; i >= 0; --i) {
        Bot * bot = bots->at(i);
        if(!bot->isValid()){
            bots->removeAt(i);
            delete bot;
        }
    }
    updatingList = false;
}

void Communication::dataIncoming() {
    while(socket->hasPendingDatagrams()){
        while(updatingList);
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        socket->readDatagram(datagram.data(), datagram.size(),
                             &sender, &senderPort);
        qDebug() << "Received datagram: " << QString(datagram) << senderPort;
        if(this->doSearch) {
            // f.e.: "SWAG\\Sw4gB0t\\50888"
            QStringList args = QString(datagram).split("\\");
            if(args.at(0) == QString("SWAG") && args.length() >= 3){
                QString name = args.at(1);
                quint16 ctlPort = (quint16)args.at(2).toInt();
                Bot * newBot = new Bot(name, sender, ctlPort);
                insert(bots, newBot);
            }
        }
    }
}

void Communication::dataIncomingClient() {
    while(clientSocket->hasPendingDatagrams()){
        while(updatingList);
        QByteArray datagram;
        datagram.resize(clientSocket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        clientSocket->readDatagram(datagram.data(), datagram.size(),
                             &sender, &senderPort);
        if(connectedBot != nullptr && QString(datagram).contains(QString(":ok"))) {
            connectedBot->updateTimestamp();
        }
    }
}

void Communication::sendData() {
    if(connectedBot != nullptr) {
        int left = static_cast<int>((gamepad->yAxis() * 100) + gamepad->xAxis()*gamepad->yAxis()*100);
        int right = static_cast<int>((gamepad->yAxis() * 100) - (gamepad->xAxis()*gamepad->yAxis()*100));
        int rot = static_cast<int>(gamepad->rotation() * 100);
        if(left > 100){
            left = 100;
        }
        if(right > 100){
            right = 100;
        }
        if(left < -100){
            left = -100;
        }
        if(right < -100){
            right = -100;
        }
        QByteArray sendData;
        if(rot == 0){
            sendData = QByteArray((QString("$M") +
                       QString::number(left) + QString(":") +
                       QString::number(right)).toStdString().c_str());
        } else {
            sendData = QByteArray((QString("$R") +
                       QString::number(rot)).toStdString().c_str());
        }
        socket->writeDatagram(sendData, connectedBot->address(), connectedBot->port());
        socket->flush();

        if(!connectedBot->isValid()){
            errorMsgString = QString("Timeout");
            removeConnectedBot();
        } else if(connectedBot->isDelayed()){
            errorMsgString = QString("poor connection");
        } else {
            errorMsgString = QString("");
        }
    }
}




