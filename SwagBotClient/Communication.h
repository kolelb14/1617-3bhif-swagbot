#pragma once

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include <QTimer>
#include "Gamepad.h"
#include "Bot.h"

class Communication : public QObject {
    Q_OBJECT
public:
    explicit Communication(QObject * parent = nullptr);
    ~Communication();
    Q_INVOKABLE void searchForClients(bool doSearch); // search for clients from QML
    Q_INVOKABLE void connectToBot(QString name); // connect from QML
    Q_INVOKABLE void connectToBot(QString ip, QString port);
    Q_INVOKABLE QStringList getClientsStringList(); // get client list from UML
    Q_INVOKABLE void setGamepadY(float y_axis);
    Q_INVOKABLE void setGamepadX(float x_axis);
    Q_INVOKABLE void setGamepadRotation(float rot);
    Q_INVOKABLE void removeConnectedBot();
    Q_INVOKABLE QString clientIP();
    Q_INVOKABLE QString errorMsg();
    Q_INVOKABLE void delErrorMsg();
private:
    void insert(QList<Bot*> * bots, Bot * bot);
    void confirm(QHostAddress addr, QString msg);
    QUdpSocket * socket; //SOCKET
    QUdpSocket * clientSocket;
    QList<Bot*> * bots; // bots
    bool doSearch; // searching?
    QTimer * timer; // timer for updates
    bool updatingList; // Is list currently updating?
    Bot * connectedBot; // The connected bot
    Gamepad * gamepad;
    QTimer * sendTimer; // sendInterval
    QString errorMsgString;
signals:
public slots:
    void dataIncoming();
    void dataIncomingClient();
private slots:
    void updateList(); // for timer
    void sendData();
};
