import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtWebEngine 1.0
import QtGamepad 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("SwagBot Client")

    Gamepad {
        id: gamepad
        deviceId: GamepadManager.connectedGamepads.length > 0 ? GamepadManager.connectedGamepads[0] : -1
        onDeviceIdChanged: GamepadManager.setCancelConfigureButton(deviceId, GamepadManager.ButtonStart)
        onButtonLeftChanged: {
            swipeView.currentIndex = 0
        }
        onButtonRightChanged: {
            swipeView.currentIndex = 1
        }
        onButtonGuideChanged: {
            updateClientsList();
            if(clientsList.children.length > 0) {
                connectToBot(clientsList.children[0].text)
            }
        }
    }

    Timer {
        interval: 20
        running: true
        repeat: true
        onTriggered: {
            var yVal = -gamepad.buttonR2 + gamepad.buttonL2
            var xVal = -gamepad.axisLeftX
            communication.setGamepadX(xVal)
            communication.setGamepadY(yVal)
            var rot = gamepad.axisRightX
            if(rot > -0.1 && rot < 0.1) {
                rot = 0
            }
            communication.setGamepadRotation(rot)
        }
    }

    header: Row {
        Label {
            id: lbStatus
            text: "Not Connected"
            padding: 15
        }
        Button {
            id: btnDisconnect
            text: "Disconnect"
            visible: false
            onClicked: disconnect()
        }
        Label {
            id: lbErrors
            text: ""
            padding: 15
            color: "red"
            font.bold: true
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 1

        Page {
            id: mainpl
            WebEngineView {
                id: webview
                anchors.fill: parent
                url: "qrc:/default.html"
                focus: true
            }
        }
        Page {
            id: selectPl
            padding: 20
            Column {
                id: select

                Text {
                    id: availableBots
                    text: qsTr("Available Bots")
                    font.pointSize: 15
                    padding: 20
                }
                Column {
                    id: clientsList
                    Timer {
                        id: columnRefreshTimer
                        interval: 1000
                        running: true
                        repeat: true
                        onTriggered: {
                            if(swipeView.currentIndex == 1) {
                                updateClientsList()
                            }
                            lbErrors.text = communication.errorMsg()
                        }
                    }
                }
                Text {
                    id: manualConnect
                    text: qsTr("Manual Connect")
                    font.pointSize: 15
                    padding: 20
                }
                Pane {
                    padding: 15
                    Row {
                        Text {
                            text: qsTr("IP:")
                            padding: 10
                            height: 30
                        }

                        TextField {
                            id: tfIp
                            Layout.fillWidth: true
                        }

                        Text {
                            text: qsTr("Port:")
                            padding: 10
                            height: 30
                        }

                        TextField {
                            id: tfPort
                        }

                        Pane {

                        }

                        Button {
                            id: btnConnectManual
                            text: qsTr("Confirm")
                            anchors.leftMargin: 10
                            onClicked: {
                                if(tfIp.text.length >= 8 && tfPort.text.length >= 1){
                                    connectToBotManual(tfIp.text, tfPort.text)
                                    swipeView.currentIndex = 0
                                    tfIp.text = qsTr("")
                                    tfPort.text = qsTr("")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Control")
            onFocusChanged: {
                swipeView.currentIndex = 1
            }
        }
        TabButton {
            text: qsTr("Connect")
            onFocusChanged: {
                swipeView.currentIndex = 0
            }
        }
    }

    function updateClientsList() {
        var clients = communication.getClientsStringList()
        var oldObjects = clientsList.children
        for (var i = 0; i < oldObjects.length; ++i) {
            oldObjects[i].destroy()
        }

        for (var i = 0; i < clients.length; ++i) {
            var object  = Qt.createQmlObject("import QtQuick 2.7;" +
                                             "import QtQuick.Controls 2.0; " +
                                             "RadioButton{"+
                                             "onClicked:{connectToBot(\"" + qsTr(clients[i]) + "\");" +
                                             "}"+
                                             "anchors.left: clientsList.left; anchors.leftMargin: 14;}",
                                             clientsList,
                                             qsTr(clients[i]))
            object.text = qsTr(clients[i])
            var string = qsTr("halloo")
        }


        if(clients.length == 0) {
            Qt.createQmlObject("import QtQuick 2.7;import QtQuick.Controls 2.0;Label{text:\"     None Available\"}",
                               clientsList, "noneAvailableLabel")
        }
    }

    function connectToBot(name){
        swipeView.currentIndex = 0
        communication.connectToBot(name)
        communication.delErrorMsg()
        lbStatus.text = "Connected to " + name
        webview.url = communication.clientIP()
        btnDisconnect.visible = true
    }

    function connectToBotManual(ip, port){
        swipeView.currentIndex = 0
        lbStatus.text = "Connected to " + ip + ":" + port
        communication.connectToBot(ip, port)
        communication.delErrorMsg()
        webview.url = "http://" + ip;
        btnDisconnect.visible = true
    }

    function disconnect() {
        communication.removeConnectedBot()
        communication.delErrorMsg()
        webview.url = "qrc:/default.html";
        btnDisconnect.visible = false
        lbStatus.text = "Not Connected"
    }    

    Connections {
    }
}
