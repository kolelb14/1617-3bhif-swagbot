#include "Bot.h"

Bot::Bot(QString name, QHostAddress address,
         quint16 port) :
    _name(name),
    _address(address),
    _port(port),
    _timestamp(QDateTime::currentMSecsSinceEpoch())
{

}

bool Bot::isEqualTo(Bot * bot) const {
    return bot->name() == this->_name &&
       bot->address() == this->_address &&
       bot->port() == this->_port;
}

QString Bot::name() const {
    return this->_name;
}

QHostAddress Bot::address() const {
    return this->_address;
}

quint16 Bot::port() const {
    return this->_port;
}

bool Bot::isValid() const {
    return (QDateTime::currentMSecsSinceEpoch() - _timestamp) < 10E3;
}

bool Bot::isDelayed() const {
    return (QDateTime::currentMSecsSinceEpoch() - _timestamp) > 2E3;
}

void Bot::updateTimestamp() {
    this->_timestamp = QDateTime::currentMSecsSinceEpoch();
}
